const express = require('express');
const app = express();
const JSON = require('./models/savedJSON');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('connected to the test database');
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  console.log(req.query);
  JSON.create(req.query);
  res.send(console.log('document was successfully created'));
});

app.get('/:uniqueID', (req, res) => {
  console.log(req.query);
  // JSON.findOne()
})

var port = process.env.PORT || 3154;

app.listen(port, function () {
  console.log('Ready to mess with some JSON on port ' + port + '!');
});