const mongoose = require("mongoose");
// check this, see if u need separate database for users. sam didnt use one
if (process.env.NODE_ENV == "production") {
  console.log('production mode');
} else {
  mongoose.connect("mongodb://localhost/savedJSON");
}

mongoose.Promise = Promise;

module.exports = mongoose;
